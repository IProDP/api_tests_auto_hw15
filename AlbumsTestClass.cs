﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RestSharp;

namespace APITest
{
    [TestFixture]
    public class AlbumsTestClass
    {
    
        [Test]
        public static void GetAlbums()
        {
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums/1");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);

            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual("1", content.userId);
            Assert.AreEqual("1", content.id);
            Assert.AreEqual("quidem molestiae enim", content.title);
        }

        [Test]
        public static void PostAlbums()
        {
            string jsonString = @"{""userId"": ""101"",""id"": ""101"",""title"":""test string""}";
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums");
            var restRequest = restApi.CreatePostRequest(jsonString);
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);

            Assert.AreEqual("Created", response.StatusCode.ToString());
            Assert.AreEqual("101", content.userId);
            Assert.AreEqual("101", content.id);
            Assert.AreEqual("test string", content.title);

        }

        [TestCase("101", "101", "test string qq")]
        [TestCase("102", "101", "test string ww")]
        [TestCase("103", "101", "test string ee")]
        [TestCase("104", "101", "test string rr")]
        [TestCase("105", "101", "test string tt")]
        [TestCase("106", "101", "test string yy")]
        [TestCase("107", "101", "test string uu")]
        public static void PostAlbumsDictinary(string userIdT, string idT, string titleT)
        {
            Dictionary<string, string> body = new Dictionary<string, string>
            {
                {"userId" , userIdT},
                {"id", idT},
                {"title", titleT}

            };
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums");
            var restRequest = restApi.CreatePostRequestDictionary(body);
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);

            Assert.AreEqual("Created", response.StatusCode.ToString());
            Assert.AreEqual(userIdT, content.userId);
            Assert.AreEqual(idT, content.id);
            Assert.AreEqual(titleT, content.title);

        }

        [TestCase("101", "2", "test string qq")]
        [TestCase("102", "2", "test string ww")]
        [TestCase("103", "2", "test string ee")]
        [TestCase("104", "2", "test string rr")]
        [TestCase("105", "2", "test string tt")]
        [TestCase("106", "2", "test string yy")]
        [TestCase("107", "2", "test string uu")]
        public static void PutAlbums(string userIdT, string idT, string titleT)
        {
            Dictionary<string, string> body = new Dictionary<string, string>
            {
                {"userId" , userIdT},
                {"id", idT},
                {"title", titleT}

            };

            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums/2");
            var restRequest = restApi.CreatePutRequestDictionary(body);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);

            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual(userIdT, content.userId);
            Assert.AreEqual(idT, content.id);
            Assert.AreEqual(titleT, content.title);
        }

        [Test]
        public static void PatchAlbums()
        {
            string jsonString = @"{""title"": ""test string""}";
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums/5");
            var restRequest = restApi.CreatePatchRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);

            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual("test string", content.title);
            Assert.AreEqual("1", content.userId);
            Assert.AreEqual("5", content.id);
        }

        [Test]
        public static void DeleteAlbums()
        {
            RestApiHelper<Albums> restApi = new RestApiHelper<Albums>();
            var restUrl = restApi.SetUrl("/albums/1");
            var restRequest = restApi.CreateDeleteRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Albums content = restApi.GetContent<Albums>(response);

                        
            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual(null, content.userId);
            Assert.AreEqual(null, content.id);
            Assert.AreEqual(null, content.title);
        }

    }
}
