﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace APITest
{
    public class RestClass
    {
        public Albums TryGetAlbums()
        {
            var restClient = new RestClient("https://jsonplaceholder.typicode.com/");
            var restRequest = new RestRequest($"/albums", Method.GET);

            restRequest.AddHeader("Accept", "application/json");
            restRequest.RequestFormat = DataFormat.Json;

            IRestResponse response = restClient.Execute(restRequest);
            var content = response.Content;

            Albums albums = JsonConvert.DeserializeObject<Albums>(content);
            return albums;
        }

        public Comments TryGetPhotos()
        {
            var restClient = new RestClient("https://jsonplaceholder.typicode.com/");
            var restRequest = new RestRequest($"/photos", Method.GET);

            restRequest.AddHeader("Accept", "application/json");
            restRequest.RequestFormat = DataFormat.Json;

            IRestResponse response = restClient.Execute(restRequest);
            var content = response.Content;

            Comments photos = JsonConvert.DeserializeObject<Comments>(content);
            return photos;
        }

    }
}

