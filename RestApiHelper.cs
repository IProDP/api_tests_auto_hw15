﻿using RestSharp;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace APITest
{
    public class RestApiHelper<T>
    {
        public RestClient _restClient;
        public RestRequest resrApiRequest;
        public string _baseUrl = "https://jsonplaceholder.typicode.com";

        public RestClient SetUrl(string resourceUrl)
        {
            RestClient _restClient = new RestClient(_baseUrl + resourceUrl);
            return _restClient;
        }

        public RestRequest CreatePostRequest(string jsonString)
        {
            resrApiRequest = new RestRequest(Method.POST);
            resrApiRequest.AddHeader("Accept", "application/json");
            resrApiRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
            return resrApiRequest;
        }

        public RestRequest CreatePostRequestDictionary(Dictionary <string, string> body)
        {
            resrApiRequest = new RestRequest(Method.POST);
            resrApiRequest.AddHeader("Accept", "application/json");
            resrApiRequest.AddJsonBody(body);
            return resrApiRequest;
        }

        public RestRequest CreatePutRequest(string jsonString)
        {
            resrApiRequest = new RestRequest(Method.PUT);
            resrApiRequest.AddHeader("Accept", "application/json");
            resrApiRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
            return resrApiRequest;
        }

        public RestRequest CreatePutRequestDictionary(Dictionary<string, string> body)
        {
            resrApiRequest = new RestRequest(Method.PUT);
            resrApiRequest.AddHeader("Accept", "application/json");
            resrApiRequest.AddJsonBody(body);
            return resrApiRequest;
        }

        public RestRequest CreatePatchRequest(string jsonString)
        {
            resrApiRequest = new RestRequest(Method.PATCH);
            resrApiRequest.AddHeader("Accept", "application/json");
            resrApiRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
            return resrApiRequest;
        }

        public RestRequest CreateGetRequest()
        {
            resrApiRequest = new RestRequest(Method.GET);
            resrApiRequest.AddHeader("Accept", "application/json");
            return resrApiRequest;
        }

        public RestRequest CreateDeleteRequest()
        {
            resrApiRequest = new RestRequest(Method.DELETE);
            resrApiRequest.AddHeader("Accept", "application/json");
            return resrApiRequest;
        }

        public IRestResponse GetResponse(RestClient restClient, RestRequest restRequest)
        {
            return restClient.Execute(restRequest);
        }

        public DTO GetContent<DTO>(IRestResponse response)
        {
            var content = response.Content;
            DTO deseiralizeObject = Newtonsoft.Json.JsonConvert.DeserializeObject<DTO>(content);
            return deseiralizeObject;
        }
    }
}
