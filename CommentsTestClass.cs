﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RestSharp;

namespace APITest
{
    [TestFixture]
    public class CommentsTestClass
    {
        [Test]
        public static void GetComments()
        {
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/comments/5");
            var restRequest = restApi.CreateGetRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Comments content = restApi.GetContent<Comments>(response);

            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual("1", content.postId);
            Assert.AreEqual("5", content.id);
            Assert.AreEqual("vero eaque aliquid doloribus et culpa", content.name);
            Assert.AreEqual("Hayden@althea.biz", content.email);
            Assert.AreEqual("harum non quasi et ratione\ntempore iure ex voluptates in ratione\nharum architecto fugit inventore cupiditate\nvoluptates magni quo et", content.body);
        }

        [Test]
        public static void PostComments()
        {
            string jsonString = @"{""postId"": ""101"",
                                       ""id"": ""501"",
                                     ""name"":""test Name"",
                                    ""email"":""PostComments@test.com"",
                                     ""body"":""Comments like""}";
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/comments");
            var restRequest = restApi.CreatePostRequest(jsonString);
            var response = restApi.GetResponse(restUrl, restRequest);
            Comments content = restApi.GetContent<Comments>(response);

            Assert.AreEqual("Created", response.StatusCode.ToString());
            Assert.AreEqual("101", content.postId);
            Assert.AreEqual("501", content.id);
            Assert.AreEqual("test Name", content.name);
            Assert.AreEqual("PostComments@test.com", content.email);
            Assert.AreEqual("Comments like", content.body);

        }

        [Test]
        public static void UpdateComments()
        {
            string jsonString = @"{""postId"": ""666"",
                                       ""id"": ""5"",
                                     ""name"":""Update Name"",
                                    ""email"":""Update@test.com"",
                                     ""body"":""Update Comments like""}";
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/comments/5");
            var restRequest = restApi.CreatePutRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Comments content = restApi.GetContent<Comments>(response);

            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual("666", content.postId);
            Assert.AreEqual("5", content.id);
            Assert.AreEqual("Update Name", content.name);
            Assert.AreEqual("Update@test.com", content.email);
            Assert.AreEqual("Update Comments like", content.body);
        }

        [Test]
        public static void PatchComments()
        {
            string jsonString = @"{""body"": ""PatchComments""}";
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/comments/5");
            var restRequest = restApi.CreatePatchRequest(jsonString);
            restRequest.RequestFormat = DataFormat.Json;
            var response = restApi.GetResponse(restUrl, restRequest);
            Comments content = restApi.GetContent<Comments>(response);

            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual("1", content.postId);
            Assert.AreEqual("5", content.id);
            Assert.AreEqual("vero eaque aliquid doloribus et culpa", content.name);
            Assert.AreEqual("Hayden@althea.biz", content.email);
            Assert.AreEqual("PatchComments", content.body);
        }

        [Test]
        public static void DeleteComments()
        {
            RestApiHelper<Comments> restApi = new RestApiHelper<Comments>();
            var restUrl = restApi.SetUrl("/comments/5");
            var restRequest = restApi.CreateDeleteRequest();
            var response = restApi.GetResponse(restUrl, restRequest);
            Comments content = restApi.GetContent<Comments>(response);


            Assert.AreEqual("OK", response.StatusCode.ToString());
            Assert.AreEqual(null, content.postId);
            Assert.AreEqual(null, content.id);
            Assert.AreEqual(null, content.name);
            Assert.AreEqual(null, content.email);
            Assert.AreEqual(null, content.body);
        }

    }
}

